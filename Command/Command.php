<?php


namespace DesignPattern\Command;

/**
 * 命令类
 * Class Command
 * @package DesignPattern\Command
 */

class Command implements CommandInterface
{
    /**
     * 接收者
     * @var Receiver
     */
    protected $receiver;

    public function __construct(Receiver $receiver)
    {
        $this->receiver = $receiver;
    }

    /**
     * @inheritDoc
     */
    public function execute()
    {
        echo get_class() . " -> 接收到命令~\n";
        $this->receiver->action();
    }
}