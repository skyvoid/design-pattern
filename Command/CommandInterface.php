<?php


namespace DesignPattern\Command;

/**
 * 命令抽象接口
 * Interface CommandInterface
 * @package DesignPattern\Command
 */

interface CommandInterface
{
    /**
     * 命令执行方法
     * @return mixed
     */
    public function execute();
}