<?php


namespace DesignPattern\Command;

/**
 * 命令发出者
 * Class Invoker
 * @package DesignPattern\Command
 */

class Invoker
{
    /**
     * 具体命令
     * @var Command
     */
    protected $command;

    public function __construct(Command $command)
    {
        $this->command = $command;
    }

    /**
     * 发出命令
     */
    public function run()
    {
        echo get_class() . " -> 发出命令~\n";
        $this->command->execute();
    }
}