<?php


namespace DesignPattern\Command;

/**
 * 命令接收者
 * Class Receiver
 * @package DesignPattern\Command
 */

class Receiver
{
    /**
     * 接收到命令后执行
     */
    public function action()
    {
        echo get_class() . " -> 开始执行~\n";
    }
}