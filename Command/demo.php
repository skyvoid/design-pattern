<?php
$receiver = new \DesignPattern\Command\Receiver();
$command = new \DesignPattern\Command\Command($receiver);
$invoke = new \DesignPattern\Command\Invoker($command);
$invoke->run();