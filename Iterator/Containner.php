<?php

namespace DesignPattern\Iterator;

/**
 * 容器基类
 * Class Containner
 * @package DesignPattern\Iterator
 */

abstract class Containner
{
    /**
     * 存储需要
     * @var
     */
    protected $dataList = [];


    /**
     * 向容器添加元素
     * @return mixed
     */
    abstract public function add();

    /**
     * 获取迭代器
     * @return mixed
     */
    abstract public function getIterator();
}