<?php

namespace DesignPattern\Iterator;

/**
 * 迭代器抽象类
 * Class Iterator
 * @package DesignPattern\Iterator
 */

abstract class Iterator
{
    /**
     * 迭代元素列表
     * @var array
     */
    protected $itemList = [];

    /**
     * 当前迭代位置
     * @var int
     */
    protected $index = 0;

    /**
     * 迭代器数据总长
     * @var int
     */
    protected $length = 0;

    /**
     * 构造函数
     * PeopleIterator constructor.
     */
    public function __construct($itemList)
    {
        $this->length = count($itemList);
        if ($this->length) {
            $this->itemList = $itemList;
        }
    }

    /**
     * 判断是否存在下一个元素
     * @return mixed
     */
    abstract protected function hasNext();

    /**
     * 迭代器返回下一个元素
     * @return mixed
     */
    abstract public function next();

    /**
     * 迭代器返回当前元素
     * @return mixed
     */
    abstract public function current();

    /**
     * 遍历迭代器所有元素
     * @return mixed
     */
    abstract public function visitor();
}