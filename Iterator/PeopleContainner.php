<?php


namespace DesignPattern\Iterator;


class PeopleContainner extends Containner
{

    /**
     * @inheritDoc
     */
    public function add($name = '')
    {
        array_push($this->dataList, $name);
    }

    /**
     * @inheritDoc
     */
    public function getIterator()
    {
        return new PeopleIterator($this->dataList);
    }
}