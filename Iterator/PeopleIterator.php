<?php


namespace DesignPattern\Iterator;


class PeopleIterator extends Iterator
{
    /**
     * @inheritDoc
     */
    protected function hasNext()
    {
        if (($this->index + 1) < $this->length) {
            return true;
        }
        return false;
    }

    /**
     * @inheritDoc
     */
    public function next()
    {
        if ($this->length) {
            if ($this->hasNext()) {
                $this->index++;
            }
            return $this->itemList[$this->index];
        } else {
            return [];
        }
    }

    /**
     * @inheritDoc
     */
    public function current() {

        if ($this->length) {
            return $this->itemList[$this->index];
        } else {
            return [];
        }
    }

    /**
     * @inheritDoc
     */
    public function visitor()
    {
        if ($this->length) {
            echo $this->current();
            while ($this->hasNext()){
                echo $this->next();
            }
            $this->index = 0;
        } else {
            echo '空容器~';
        }
    }
}