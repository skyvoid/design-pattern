<?php

namespace DesignPattern\ObMod;

/**
 * 具体观察对象
 * Class EmailObserve
 * @package DesignPattern\ObMod
 */

class EmailObserve implements Observe
{
    public function update()
    {
        echo "邮件已发出~\n";
    }
}