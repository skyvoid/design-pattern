<?php


namespace DesignPattern\ObMod;

/**
 * 具体被观察主题
 * Class MessageSubject
 * @package DesignPattern\ObMod
 */

class MessageSubject extends SubjectAbstract
{
    /**
     * 被观察主题发出一个动作
     */
    public function publish()
    {
        echo "我发出了一个通知~\n";
        $this->notify();
    }
}