<?php

namespace DesignPattern\ObMod;

/**
 * 抽象观察者
 * Interface Observe
 * @package DesignPattern\ObMod
 */

interface Observe
{
    /**
     * 状态更新方法
     * @return mixed
     */
    public function update();
}