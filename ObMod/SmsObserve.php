<?php

namespace DesignPattern\ObMod;

/**
 * 具体观察对象
 * Class SmsObserve
 * @package DesignPattern\ObMod
 */

class SmsObserve implements Observe
{
    public function update()
    {
        echo "短信通知发出~\n";
    }
}