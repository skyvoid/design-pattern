<?php

namespace DesignPattern\ObMod;

/**
 * 抽象角色
 * Class SubjectAbstract
 * @package DesignPattern\ObMod
 */

abstract class SubjectAbstract
{
    /**
     * 观察对象集合
     * @var array
     */
    private $observes = [];

    /**
     * 构造函数
     * SubjectAbstract constructor.
     * @param Observe $observe
     */
    public function __construct(Observe $observe = null)
    {
        if (!is_null($observe)) {
            array_push($this->observes, $observe);
        }
    }

    /**
     * 添加观察对象
     * @param Observe $observe
     */
    public function attach(Observe $observe)
    {
        if (!is_null($observe)) {
            array_push($this->observes, $observe);
        }
    }

    /**
     * 更新观察对象状态
     */
    protected function notify()
    {
        if (count($this->observes)){
            foreach ($this->observes as $observe){
                $observe->update();
            }
        }
    }
}