<?php


namespace DesignPattern\ObMod;


class WeChatObserve implements Observe
{
    public function update()
    {
        echo "微信消息已发出~\n";
    }
}