<?php
$message = new \DesignPattern\ObMod\MessageSubject();
$sms = new \DesignPattern\ObMod\SmsObserve();
$email = new \DesignPattern\ObMod\EmailObserve();
$email = new \DesignPattern\ObMod\EmailObserve();
$wechat = new \DesignPattern\ObMod\WeChatObserve();
$message->attach($sms);
$message->attach($email);
$message->attach($wechat);
$message->publish();