<?php


namespace DesignPattern\Proxy;

/**
 * 代理类
 * Class Proxy
 * @package DesignPattern\Proxy
 */

class Proxy implements Subject
{
    /**
     * 主题引用
     * @var Subject
     */
    protected $subject;

    /**
     * 初始化真实主题代理
     * Proxy constructor.
     * @param Subject $subject
     */
    public function __construct(Subject $subject)
    {
        $this->subject = $subject;
    }

    /**
     * 实现代理方法
     * @inheritDoc
     */
    public function action()
    {
        echo get_class() . " -> 代理主题调用\n";
        $this->subject->action();
    }
}