<?php


namespace DesignPattern\Proxy;

/**
 * 真实代理主题
 * Class RealSubject
 * @package DesignPattern\Proxy
 */

class RealSubject implements Subject
{

    /**
     * 实现主题操作方法
     * @inheritDoc
     */
    public function action()
    {
        echo get_class() . " -> 实际主题调用\n";
    }
}