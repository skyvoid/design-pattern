<?php


namespace DesignPattern\Proxy;

/**
 * 代理抽象主题
 * Interface Subject
 * @package DesignPattern\Proxy
 */

interface Subject
{
    /**
     * 主题抽象方法
     * @return mixed
     */
    public function action();
}