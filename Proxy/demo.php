<?php
$realsubject = new \DesignPattern\Proxy\RealSubject();
$proxy = new \DesignPattern\Proxy\Proxy($realsubject);
$proxy->action();