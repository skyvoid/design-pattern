<?php


namespace DesignPattern\Template;

/**
 * 根据模板生成的华为手机
 * Class Huawei
 * @package DesignPattern\Template
 */

class Huawei extends Phone
{

    protected function showLogo()
    {
        echo "华为 logo~\n";
    }
}