<?php


namespace DesignPattern\Template;

/**
 * 手机模板基类
 * Class Phone
 * @package DesignPattern\Template
 */

abstract class Phone
{
    /**
     * 固定的开机流程模板
     */
    final public function action()
    {
        $this->powerOn();
        $this->showLogo();
        $this->callNumber();
    }

    /**
     * 固定流程开机
     */
    protected function powerOn()
    {
        echo "开机~\n";
    }

    /**
     * 不同手机自定义logo
     * @return mixed
     */
    abstract protected function showLogo();

    /**
     * 固定流程打电话
     */
    protected function callNumber() {
        echo "呼叫中...\n";
    }
}