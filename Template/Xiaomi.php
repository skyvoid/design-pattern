<?php


namespace DesignPattern\Template;

/**
 * 根据模板生成的小米手机
 * Class Xiaomi
 * @package DesignPattern\Template
 */

class Xiaomi extends Phone
{

    protected function showLogo()
    {
        echo "小米 logo~\n";
    }
}