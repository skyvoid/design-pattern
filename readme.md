# PHP设计模式
---
+ Command -- 命令模式
+ Iterator -- 迭代器模式
+ ObMod -- 观察者模式
+ Proxy -- 代理模式
+ Template -- 模板模式
---
```

cd ./DesignPattern
chcp 65001 // 修正控制台编码为utf8（65001） 编码936为gbk 
php run.php ObMod

```
![输出观察者模式结果](https://cdn-img.taoke.com/5e9db60ef3e494d2cafb53d86e13d31e "观察者模式")
---
### 附录git命令
---
![](https://upload-images.jianshu.io/upload_images/4631036-77e761fc020d461f.png)