<?php
$mod = $argv[1];
$rootPath = dirname(__FILE__);

spl_autoload_register(function ($class) use ($rootPath, $mod) {
    $filePath = $rootPath . DIRECTORY_SEPARATOR . $mod . DIRECTORY_SEPARATOR . basename($class) . '.php';
    if (is_readable($filePath)) {
        include_once $filePath;
    }
});

include_once $rootPath . DIRECTORY_SEPARATOR . $mod . DIRECTORY_SEPARATOR . "demo.php";